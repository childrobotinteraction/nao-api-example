# README #

NAO JAVA API Example for the master course Intelligent User eXperience Engineering (IUXE)

### What is this repository for? ###

* Goal: inspire student groups working on IUXE projects to use the (Java) API of the NAO.
* Version 1.0

### Dependencies ###

* java-naoqi-sdk-2.1.4.13-win32-vs2010

### Notes for use ###

* The example contains a RobotScriptActor who can execute a RobotScript.
* The RobotScript can be parsed from an external text file.
* Every line in the file is processed (parsed and executed) sequentially.
* This example of RobotScript contains only two modes of action: AnimatedSay (saying text while making gestures) and performing a predefined behavior.
* RobotScript makes use of Aldebaran's native ALAnimatedSpeech (http://doc.aldebaran.com/2-1/naoqi/audio/alanimatedspeech.html)
* With the addition that behaviors can be loaded by adding them between \<behavior>...\</behavior> tags
* The behaviors you want to run need to be installed on the Nao first (http://doc.aldebaran.com/2-4/naoqi/core/albehaviormanager.html)

### Contact ###

* e-mail: m.e.u.ligthart@tudelft.nl