/*
 * Copyright (c) 2017. Mike Ligthart
 * PhD Candidate, Delft University of Technology
 */

package nl.tudelft.ii;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by mike on 08-Feb-17.
 */
public class RobotScriptParser {

    public LinkedList<RobotScriptElement> parse(String filePath) throws RobotScriptParserException {
        LinkedList<RobotScriptElement> output;
        try (Stream<String> lines = Files.lines(Paths.get(filePath))) {
            output = lines.map(this::toRobotScriptElement)
            .collect(Collectors.toCollection(LinkedList::new));
        } catch (IOException e) {
            throw new RobotScriptParserException("Failed Failed to collect lines due to IOException", e.getCause());
        }

        if (output == null){
            throw new RobotScriptParserException("Failed to collect lines from " + filePath);
        }
        return output;
    }

    private RobotScriptElement toRobotScriptElement(String line){
        final Pattern pattern = Pattern.compile("<behavior>(.+?)</behavior>");
        final Matcher matcher = pattern.matcher(line);
        if (matcher.find()){
            return new RobotScriptElement(RobotScriptElement.Type.BEHAVIOR, matcher.group(1));
        }
        return new RobotScriptElement(RobotScriptElement.Type.ANIMATEDSPEECH, line);
    }

}
