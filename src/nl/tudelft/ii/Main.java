/*
 * Copyright (c) 2017. Mike Ligthart
 * PhD Candidate, Delft University of Technology
 */

package nl.tudelft.ii;

import java.util.LinkedList;

public class Main {

    public static void main(String[] args) {
        String robotUrl = "tcp://nao.local:9559"; // tcp://{NAME}.{IP-ADDRESS; Default (when on same network): local}:{PORT; Default:9559}
        String robotScriptFilePath = "C:\\Users\\mike\\IdeaProjects\\StorytellingPromo\\src\\nl\\tudelft\\ii\\scripts\\storytellingPromo.script.txt";

        try (RobotScriptActor robot = new RobotScriptActor(args, robotUrl)) {
            LinkedList<RobotScriptElement> robotScript = new RobotScriptParser().parse(robotScriptFilePath);
            robot.initialize();
            robot.performScript(robotScript);
        } catch (RobotScriptParserException e) {
            e.printStackTrace();
        } catch (RobotScripActorException e) {
            e.printStackTrace();
        }
    }
}
