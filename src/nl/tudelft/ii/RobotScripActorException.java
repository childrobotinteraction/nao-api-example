/*
 * Copyright (c) 2017. Mike Ligthart
 * PhD Candidate, Delft University of Technology
 */

package nl.tudelft.ii;


public class RobotScripActorException extends Exception {

    public RobotScripActorException() {
        super();
    }

    public RobotScripActorException(String message) {
        super(message);
    }

    public RobotScripActorException(String message, Throwable cause) {
        super(message, cause);
    }

    public RobotScripActorException(Throwable cause) {
        super(cause);
    }

    protected RobotScripActorException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
