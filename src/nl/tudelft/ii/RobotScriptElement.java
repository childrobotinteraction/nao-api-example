/*
 * Copyright (c) 2017. Mike Ligthart
 * PhD Candidate, Delft University of Technology
 */

package nl.tudelft.ii;

/**
 * Created by mike on 08-Feb-17.
 */
public class RobotScriptElement {

    public enum Type {
        ANIMATEDSPEECH, SPEECH, BEHAVIOR, MOTION;
    }

    private Type elementType;
    private String elementContent;

    public RobotScriptElement(Type elementType, String elementContent) {
        this.elementType = elementType;
        this.elementContent = elementContent;
    }

    public Type getElementType() {
        return elementType;
    }

    public String getElementContent() {
        return elementContent;
    }
}
