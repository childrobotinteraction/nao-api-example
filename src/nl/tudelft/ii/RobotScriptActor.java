/*
 * Copyright (c) 2017. Mike Ligthart
 * PhD Candidate, Delft University of Technology
 */

package nl.tudelft.ii;

import com.aldebaran.qi.Application;
import com.aldebaran.qi.CallError;
import com.aldebaran.qi.helper.proxies.ALAnimatedSpeech;
import com.aldebaran.qi.helper.proxies.ALBehaviorManager;
import com.aldebaran.qi.helper.proxies.ALMotion;
import com.aldebaran.qi.helper.proxies.ALRobotPosture;

import java.util.LinkedList;

/**
 * Created by mike on 08-Feb-17.
 */
public class RobotScriptActor implements AutoCloseable {

    private Application application;
    private ALAnimatedSpeech animatedSpeech;
    private ALBehaviorManager behaviorManager;
    private ALRobotPosture poseManager;
    private ALMotion motionManager;

    public RobotScriptActor(String[] args, String robotUrl) throws RobotScripActorException {
        application = new Application(args, robotUrl);
        application.start();

        try {
            animatedSpeech = new ALAnimatedSpeech(application.session());
        } catch (Exception e) {
            throw new RobotScripActorException("No new ALAnimatedSpeech instance could be created | " + e.getMessage(), e.getCause());
        }

        try {
            behaviorManager = new ALBehaviorManager(application.session());
        } catch (Exception e) {
            throw new RobotScripActorException("No new ALBehaviorManager instance could be created | " + e.getMessage(), e.getCause());
        }

        try {
            motionManager = new ALMotion(application.session());
        } catch (Exception e) {
            throw new RobotScripActorException("No new ALMotion instance could be created | " + e.getMessage(), e.getCause());
        }

        try {
            poseManager = new ALRobotPosture(application.session());
        } catch (Exception e) {
            throw new RobotScripActorException("No new ALRobotPosture instance could be created | " + e.getMessage(), e.getCause());
        }
    }

    public void initialize() throws RobotScripActorException {
        try {
            motionManager.wakeUp();
            poseManager.applyPosture("sit", 1.0f);

        } catch (CallError callError) {
            throw new RobotScripActorException("CallError: " + callError.getMessage(), callError.getCause());
        } catch (InterruptedException e) {
            throw new RobotScripActorException("InterruptedException: " + e.getMessage(), e.getCause());
        }
    }

    public void close() throws RobotScripActorException {
        try {
            motionManager.rest();
        } catch (CallError callError) {
            throw new RobotScripActorException("CallError: " + callError.getMessage(), callError.getCause());
        } catch (InterruptedException e) {
            throw new RobotScripActorException("InterruptedException: " + e.getMessage(), e.getCause());
        } finally {
            application.stop();
        }
    }

    public void performScript(LinkedList<RobotScriptElement> script) throws RobotScripActorException {
        for (RobotScriptElement scriptElement : script){
            switch (scriptElement.getElementType()){
                case ANIMATEDSPEECH: say(scriptElement.getElementContent()); break;
                case BEHAVIOR: behave(scriptElement.getElementContent()); break;
                default: throw new RobotScripActorException("Element type not recognized");
            }
        }
    }

    private void say(String line) throws RobotScripActorException {
        try {
            animatedSpeech.say(line);
        } catch (CallError callError) {
            throw new RobotScripActorException("CallError: " + callError.getMessage(), callError.getCause());
        } catch (InterruptedException e) {
            throw new RobotScripActorException("InterruptedException: " + e.getMessage(), e.getCause());
        }
    }

    private void behave(String behavior) throws RobotScripActorException {
        try {
            behaviorManager.runBehavior(behavior);
        } catch (CallError callError) {
            throw new RobotScripActorException("CallError: " + callError.getMessage(), callError.getCause());
        } catch (InterruptedException e) {
            throw new RobotScripActorException("InterruptedException: " + e.getMessage(), e.getCause());
        }
    }
}
