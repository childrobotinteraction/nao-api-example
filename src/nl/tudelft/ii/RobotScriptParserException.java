/*
 * Copyright (c) 2017. Mike Ligthart
 * PhD Candidate, Delft University of Technology
 */

package nl.tudelft.ii;

/**
 * Created by mike on 08-Feb-17.
 */
public class RobotScriptParserException extends Exception{
    public RobotScriptParserException() {
        super();
    }

    public RobotScriptParserException(String message) {
        super(message);
    }

    public RobotScriptParserException(String message, Throwable cause) {
        super(message, cause);
    }

    public RobotScriptParserException(Throwable cause) {
        super(cause);
    }

    protected RobotScriptParserException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
